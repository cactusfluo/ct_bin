#!/usr/bin/env bash
############################### BY GIBBONJOYEUX ################################

################################################################################
### FUNCTIONS
################################################################################

function	usage()
{
	printf "usage: %s\n" "sw SOURCE_1 SOURCE_2" >&2
	printf "Switch SOURCE_1 and SOURCE_2 (files, or directories)" >&2
}

function	error()
{
	printf "sw: ${1:-unknown error}\n" >&2
}

################################################################################
### MAIN
################################################################################

if [[ ${#} -ne 2 ]]; then
	error "requires 2 files to switch\n"
	usage
	exit 1
fi

mv "${1}" .sw_tmp
mv "${2}" "${1}"
mv .sw_tmp "${2}"
