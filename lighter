#!/usr/bin/env bash
################################################################################
# by cactusfluo
################################################################################
# Description:
#  Lighter is a bin which reduce the jpeg quality of an image in a way that the
#  image stays under a given weight.
################################################################################
# Usage:
#  > lighter IMAGE_SRC IMAGE_DST MAX_SIZE MAX_SIZE_UNIT
#
#  . IMAGE_SRC: 		Original image (ex: original_image.jpg)
#  . IMAGE_DST: 		Destination image name (ex: resulting_image.jpg)
#  . MAX_SIZE: 			Maximum size of the resulting image.
#  . MAX_SIZE_UNIT:		Unit of maximum size (b | kb | mb)
################################################################################

[[ ${CONVERT_PATH} ]] && PATH="${CONVERT_PATH}:${PATH}"

################################################################################
### FUNCTIONS
################################################################################

function	usage()
{
	if [[ ${1} == big ]]; then
		printf "Usage: lighter IMAGE_SRC IMAGE_DST MAX_SIZE MAX_SIZE_UNIT\n"
		printf "IMAGE_SRC      original image (ex: original_image.jpg)\n"
		printf "IMAGE_DST      destination image (ex: resulting_image.jpg)\n"
		printf "MAX_SIZE       maximum size of the resulting image\n"
		printf "MAX_SIZE_UNIT  unit of maximum size (b | kb | mb)\n"
	### SMALL USAGE
	elif [[ ${1} == small ]]; then
		printf "Try 'lighter --help' for more information.\n" >&2
	fi
}

function	error()
{
	printf "lighter: ${1}\n" >&2
}

################################################################################
### MAIN
################################################################################

### CHECK PARAMETERS
if [[ ${1} == --help ]]; then
	usage big
	exit 0
fi
if [[ ${#} -ne 4 ]]; then
	error "wrong number of parameter"
	usage small
	exit 1
fi
if [[ ! -f ${1} ]]; then
	error "${1} file does not exist"
	usage small
	exit 1
fi
if [[ ! ${3} =~ ^[0-9]+$ ]]; then
	error "maximum size must be a number (integer)"
	usage small
	exit 1
fi

### GET THE PARAMETERS
img_src=${1}	# THE ORIGINAL IMAGE
img_dst=${2}	# THE DESTINATION IMAGE
img_max=${3}	# THE MAXIMUM SIZE IN ko
if [[ ${4} == "b" ]]; then
	max_size=${img_max}
elif [[ ${4} == "kb" ]]; then
	max_size=$((img_max * 1024))
elif [[ ${4} == "mb" ]]; then
	max_size=$((img_max * 1024 * 1024))
else
	error "maximum size unit must be one of 'b' | 'kb' | 'mb'"
	usage small
	exit 1
fi

### CHECK IF SMALLER
if [[ $(wc -c < ${img_src}) -le ${max_size} ]]; then
	cp "${img_src}" "${img_dst}"
	printf "[100%%] ${img_src} (not modified)\n"
	exit 0
fi

### INIT VARIABLES
quality=50
padding=25
percent=0

### BEGIN LOOP
while : ; do
	### CONVERT IMAGE TO SPECIFIED QUALITY
	convert ${img_src} -quality ${quality} ${img_dst}
	if [[ $? -ne 0 ]]; then
		error "convert bin got issues"
		exit 1
	fi
	### GET SIZE
	size=$(wc -c < ${img_dst})
	((percent += 12))
	printf "[%3d%%] ${img_dst}\n" ${percent}
	#printf "quality [%3u] size [%3uko]\n" ${quality} $((size / 1024))
	### CHECK SIZE
	if [[ ${size} -eq ${max_size} ]]; then
		exit 0
	elif [[ ${size} -lt ${max_size} ]]; then
		((quality += padding))
	else
		((quality -= padding))
	fi
	[[ ${quality} -lt 1 ]] && quality=1
	[[ ${quality} -gt 100 ]] && quality=100
	### SHARPEN PADDING
	[[ ${padding} -eq 0 ]] && break
	if [[ $((padding % 2)) -eq 1 ]] && [[ ${padding} -gt 1 ]]; then
		((padding = (padding / 2) + 1))
	else
		((padding /= 2))
	fi
done

### CORRECT IF NECESSARY
if [[ ${size} -gt ${max_size} ]]; then
	if [[ ${quality} -gt 1 ]]; then
		((quality--))
		convert ${img_src} -quality ${quality} ${img_dst}
		size=$(wc -c < ${img_dst})
		printf "[ 95%%] ${img_dst}\n"
		#printf "quality [%3u] size [%3uko]\n" ${quality} $((size / 1024))
	else
		error "cannot reduce image size enough.\nThis could be due to a too big image size (in pixel)"
		exit 1
	fi
fi

printf "[100%%] ${img_dst} (${quality}%%)\n"
